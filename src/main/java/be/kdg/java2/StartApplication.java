package be.kdg.java2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class StartApplication {
    public static void main(String[] args) {
        System.out.println("Hello demo lambda!");
        List<String> names = Arrays.asList("Jos", "Jef", "Annelies", "Maria");
        //Collections.sort(names, new StringLengthComparator());
        Collections.sort(names, (s1, s2)-> s1.length()-s2.length());
        names.forEach(System.out::println);
    }
}
